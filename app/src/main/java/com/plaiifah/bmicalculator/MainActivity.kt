package com.plaiifah.bmicalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.plaiifah.bmicalculator.databinding.ActivityMainBinding
import java.text.NumberFormat
import kotlin.math.pow

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener{ calculateBMI() }
    }

    private fun calculateBMI() {
        val textHeight = binding.heightEditText.text.toString()
        val textWeight = binding.weightEditText.text.toString()
        val heightx = textHeight.toDoubleOrNull()
        val weightx = textWeight.toDoubleOrNull()
        if (heightx == null || weightx == null) {
            binding.bmiResult.text = " "
            binding.xy.text=" "
            return
        }
        val result = weightx/((heightx/100).pow(2))
        val bmi = "%.2f".format(result)
        binding.bmiResult.text = "BMI : "+bmi.toString()

        if(bmi.toDouble() < 18.50 ){
            binding.xy.text = "UNDER WEIGHT"
        }else if(bmi.toDouble() < 25 ) {
            binding.xy.text = "NORMAL "
        }else if(bmi.toDouble() < 30 ) {
            binding.xy.text = "OVER WEIGHT "
        }else if(bmi.toDouble() < 35 ) {
            binding.xy.text = "OBESE "
        }else {
            binding.xy.text = "EXTREMELY OBESE  "
        }




    }


}